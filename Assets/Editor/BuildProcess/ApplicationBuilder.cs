﻿using System;
using System.Linq;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

public class ApplicationBuilder : MonoBehaviour
{
	//出力ファイルのパス
	//プロジェクトルートからの相対パスを記載
	//拡張子は含まない
	private static string OutputPath = String.Empty;

	//AndroidのKeyStoreのパスワード
	private static string AndroidKeyStorePassPhrase = string.Empty;

	[MenuItem("Build/Android %&b")]
	public static void BuildAndroid()
	{
		ReadCommandParameter();

		BuildAndroid(OutputPath, AndroidKeyStorePassPhrase);
	}

	private static void BuildAndroid(string outputPath, string keyStorePhrase)
	{
		InputKeystorePassword(keyStorePhrase);

		var buildPlayerOptions = new BuildPlayerOptions
		{
			scenes = LoadScenePathsInBuild(),
			locationPathName = outputPath + ".apk",
			target = BuildTarget.Android,
			options = BuildOptions.None
		};

		BuildApplication(buildPlayerOptions);
	}

	[MenuItem("Build/IOS %&S")]
	public static void BuildIOS()
	{
		ReadCommandParameter();
		BuildIOS(OutputPath);
	}

	private static void BuildIOS(string outputPath)
	{
		var buildPlayerOptions = new BuildPlayerOptions
		{
			scenes = LoadScenePathsInBuild(),
			locationPathName = outputPath,
			target = BuildTarget.iOS,
			options = BuildOptions.None
		};

		BuildApplication(buildPlayerOptions);
	}

	private static void BuildApplication(BuildPlayerOptions buildPlayerOptions)
	{
		var buildReport = BuildPipeline.BuildPlayer(buildPlayerOptions);
		var buildSummary = buildReport.summary;

		if (buildSummary.result == BuildResult.Succeeded)
			Debug.Log("Build Succeeded:" + buildSummary.totalSize + "bytes");
		if (buildSummary.result == BuildResult.Failed)
			Debug.Log("Build Failed");
	}

	/// <summary>
	///     Android用に、KeyStoreのパスワードを設定する
	/// </summary>
	/// <param name="passPhrase"></param>
	private static void InputKeystorePassword(string passPhrase)
	{
		PlayerSettings.Android.keystorePass = passPhrase;
		PlayerSettings.Android.keyaliasPass = passPhrase;
	}

	[MenuItem("Build/Sample/Test1 %&z")]
	private static string[] LoadScenePathsInBuild()
	{
		var paths = EditorBuildSettings.scenes.Select(x => x.path).ToArray();

		foreach (var path in paths) Debug.Log("Path => " + path);
		return paths;
	}

	/// <summary>
	/// コマンドライン引数を読み取る処理
	/// </summary>
	private static void ReadCommandParameter()
	{
		var commandArgs = Environment.GetCommandLineArgs();

		for (var i = 0; i < commandArgs.Length; i++)
			switch (commandArgs[i])
			{
				case "-outputPath":
					//Debug.LogFormat("<-outputPath = {0}>", commandArgs[i + 1]);
					OutputPath = commandArgs[i + 1];
					break;
				case "-keystorePass":
					//Debug.LogFormat("<-keystorePass = {0}>", commandArgs[i + 1]);
					AndroidKeyStorePassPhrase = commandArgs[i + 1];
					break;
				default:
					break;
			}
	}
}