﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonEventConnecter : MonoBehaviour
{
	[SerializeField]
	private ButtonEventModel classB;

	[SerializeField]
	private Button button1;

	// Use this for initialization
	void Start()
	{
		AddListenerToButton();
	}

	private void AddListenerToButton()
	{
		button1.onClick.AddListener(() => { classB.OnButton1Clicked(); });
	}

	// Update is called once per frame
	void Update()
	{

	}
}
